# Find Computer

Find Computer adalah project aplikasi web yang dibuat untuk seleksi peserta Software Engineering Academy Compfest 12.

Stack teknologi yang digunakan:
* [Gatsby JS](https://www.gatsbyjs.org/) (Frontend)
* [Spring Boot](https://spring.io/projects/spring-boot) (Backend)
* [MySQL](https://spring.io/projects/spring-boot) (Database)
* [Amazon Web Service](https://aws.amazon.com/) (Cloud Service)
  ![Infrastruktur Find Computer](https://project-documentations.s3-ap-southeast-1.amazonaws.com/bd9a56a1-9a8a-4c0c-96e4-79ec7558cfa2.png)
  * [Grid Infrastructure Image](https://project-documentations.s3-ap-southeast-1.amazonaws.com/c25c1631-31f4-4693-96b3-15f937577650.png)
  * Elastic Compute Cloud (Server Backend)
  * Simple Storage Service (Storage untuk production build Frontend)
  * Route 53 (DNS Management System)
  * CloudFront (Web Service pembantu distribusi static web)
  * Relational Database Service (Web Service untuk database relational di cloud)
* [Paper CSS](https://www.getpapercss.com/) (Primary CSS Framework)
* [Test Cafe](https://devexpress.github.io/testcafe/) (End to End Testing)
* [JUnit](https://junit.org/junit5/) (Backend Unit Testing)
* [Gitlab](https://gitlab.com/) (Primary Repository and CI/CD Pipeline)
* [Lighthouse](https://developers.google.com/web/tools/lighthouse) (Web Application audit tool)

Repository pada project ini berbentuk Monolithic, semua stack disimpan di dalam project ini. Akan tetapi, dengan memanfaatkan gitlab CI, pipeline beserta proses deployment dilakukan secara terpisah dan paralel.

## Instalasi

Gunakan package manager [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) untuk menginstall Frontend.

```bash
# di dalam direktori frontend
npm install
```

[Maven](https://maven.apache.org/install.html) digunakan untuk membangun (build) Backend.

```bash
# di dalam direktori backend
mvn package
```

## Penggunaan

Frontend.

```bash
# di dalam direktori frontend
# development
gatsby develop

# production build
gatsby build

# production run server
gatsby serve
```

Backend.

```bash
# di dalam direktori backend
# development
mvn springboot:run

# production (.jar)
java -jar FindComputer-0.0.1-SNAPSHOT.jar
```

Lighthouse.

```bash
npx lighthouse-badges --urls https://find-computer.priambudi.fyi -o test_results
```


## License
[MIT](./LICENSE)
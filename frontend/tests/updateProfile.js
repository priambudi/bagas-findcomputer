import { Selector, Role } from 'testcafe';

const URL = 'https://find-computer.priambudi.fyi/';
const LOGIN_URL = 'https://find-computer.priambudi.fyi/login';
// const URL = 'http://localhost:8000/';
// const LOGIN_URL = 'http://localhost:8000/login';
const regularAccUser = Role(LOGIN_URL, async t => {
    await t
        .typeText('#inputEmail', 'c@c.com')
        .typeText('#inputPassword', '12345')
        .click('#login-submit');
});

fixture `Update Profile`
    .page(URL);

// test('Update Profile', async t => {
//     const loginButton = Selector('#login');
//     const cardTitle = Selector('.card-title');
//     const nameField = Selector('#inputName');
//     const updateSubmit = Selector('#update-submit');

//     const testName = 'test';
//     const postTestName = 'nama u';

//     await t
//     .setNativeDialogHandler(() => true)
//         .expect(loginButton.innerText).eql('Login')
//         .click(loginButton)
//         .expect(cardTitle.innerText).eql('Login')
//         .useRole(regularAccUser)
//         .navigateTo('/profile')
//         .expect(cardTitle.innerText).eql('Profile')
//         .selectText(nameField)
//         .pressKey('delete')
//         .typeText(nameField, testName)
//         .click(updateSubmit)
//         .navigateTo('/')
//         .navigateTo('/profile')
//         .expect(cardTitle.innerText).eql('Profile')
//         .expect(nameField.value).eql(testName)
//         .selectText(nameField)
//         .pressKey('delete')
//         .typeText(nameField, postTestName)
//         .click(updateSubmit)
//         .navigateTo('/')
//         .navigateTo('/profile')
//         .expect(cardTitle.innerText).eql('Profile')
//         .expect(nameField.value).eql(postTestName)
// });

// test('Add item', async t => {
//     const loginButton = Selector('#login');
//     const cardTitle = Selector('.card-title');
//     const modalTitle = Selector('.modal-title');
//     const addFloatingButton = Selector('#add-item');
//     const nameField = Selector('#inputNameBarang');
//     const descriptionField = Selector('#inputDescription');
//     const categoryField = Selector('#inputCategory');
//     const categoryName = Selector('#RAM');
//     const priceField = Selector('#inputPrice');
//     const addItemSubmit = Selector('#addItem-submit');

//     const testName = 'test';
//     const postTestName = 'nama u';

//     await t
//     .setNativeDialogHandler(() => true)
//         .expect(loginButton.innerText).eql('Login')
//         .click(loginButton)
//         .expect(cardTitle.innerText).eql('Login')
//         .useRole(regularAccUser)
//         .navigateTo('/profile')
//         .expect(Selector('#item-description').withExactText('test deskripsi').count).eql(0)
//         .navigateTo('/')
//         .click(addFloatingButton)
//         .expect(modalTitle.innerText).eql('Add Item')
//         .typeText(nameField, 'test barang')
//         .typeText(descriptionField, 'test deskripsi')
//         .click(categoryField)
//         .click(categoryName)
//         .typeText(priceField, '100000')
//         .click(addItemSubmit)
//         .navigateTo('/profile')
//         .expect(Selector('#item-description').withExactText('test deskripsi').count).gt(0)
// });

// test('Update item', async t => {
//     const loginButton = Selector('#login');
//     const cardTitle = Selector('.card-title');
//     const modalTitle = Selector('.modal-title');
//     const updateFloatingButton = Selector('#update-item');
//     const nameField = Selector('#inputNameBarang');
//     const categoryField = Selector('#inputCategory');
//     const categoryName = Selector('#test-barang');
//     const updateItemSubmit = Selector('#updateItem-submit');

//     await t
//     .setNativeDialogHandler(() => true)
//         .expect(loginButton.innerText).eql('Login')
//         .click(loginButton)
//         .expect(cardTitle.innerText).eql('Login')
//         .useRole(regularAccUser)
//         .navigateTo('/profile')
//         .click(updateFloatingButton)
//         .expect(modalTitle.innerText).eql('Update Item')
//         .click(categoryField)
//         .click(categoryName)
//         .expect(nameField.value).eql('test barang')
//         .selectText(nameField)
//         .pressKey('delete')
//         .typeText(nameField, 'test update barang')
//         .click(updateItemSubmit)
//         .navigateTo('/')
//         .navigateTo('/profile')
//         .click(updateFloatingButton)
//         .expect(modalTitle.innerText).eql('Update Item')
//         .click(categoryField)
//         .click(categoryName)
//         .expect(nameField.value).eql('test update barang')
// });
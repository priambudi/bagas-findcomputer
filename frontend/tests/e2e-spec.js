import { Selector, Role } from 'testcafe';

const URL = 'https://find-computer.priambudi.fyi';
const LOGIN_URL = 'https://find-computer.priambudi.fyi/login';
// const URL = 'http://localhost:8000/';
// const LOGIN_URL = 'http://localhost:8000/login';
const regularAccUser = Role(LOGIN_URL, async t => {
    await t
        .typeText('#inputEmail', 'c@c.com')
        .typeText('#inputPassword', '12345')
        .click('#login-submit');
});

fixture `Register, Log Out, Log In`
    .page(URL);

test('Site Exist', async t => {
    await t
        .expect(Selector('#title').innerText).eql('Find Computer');
});

// test('Register', async t => {
//     const loginButton = Selector('#login');
//     const registerButton = Selector('#register');
//     const cardTitle = Selector('.card-title');
//     const nameField = Selector('#inputName');
//     const emailField = Selector('#inputEmail');
//     const passwordField = Selector('#inputPassword1');
//     const confirmPasswordField = Selector('#inputPassword2');
//     const registerSubmit = Selector('#register-submit');
//     const profileButton = Selector('#profile');
//     await t
//         .expect(loginButton.innerText).eql('Login')
//         .click(loginButton)
//         .expect(cardTitle.innerText).eql('Login')
//         .click(registerButton)
//         .expect(cardTitle.innerText).eql('Register')
//         .typeText(nameField, 'test')
//         .typeText(emailField, 'test@test.com')
//         .typeText(passwordField, '12345')
//         .typeText(confirmPasswordField, '12345')
//         .click(registerSubmit)
//         .expect(profileButton.innerText).eql('Profile')
// });

// test('Login', async t => {
//     const loginButton = Selector('#login');
//     const cardTitle = Selector('.card-title');
//     const profileButton = Selector('#profile');
//     const nameField = Selector('#inputName');

//     await t
//         .expect(loginButton.innerText).eql('Login')
//         .click(loginButton)
//         .expect(cardTitle.innerText).eql('Login')
//         .useRole(regularAccUser)
//         .navigateTo('/profile')
//         .expect(nameField.value).eql('nama u')
// });

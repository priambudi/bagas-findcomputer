export const parseCategoryIdToCategory = id => {
    if (id === 1) {
      return "RAM"
    } else if (id === 2) {
      return "Processor"
    } else if (id === 3) {
      return "VGA"
    } else if (id === 4) {
      return "Motherboard"
    } else if (id === 5) {
      return "Storage"
    }
  }

export const parseCategoryToColor = cat => {
  if (cat === undefined) return "#fff"
  const category = cat.toLowerCase()
  if (category === "ram") {
    return "#2d132c"
  } else if (category === "processor") {
    return "#84142d"
  } else if (category === "vga") {
    return "#204051"
  } else if (category === "motherboard") {
    return "#3b6978"
  } else if (category === "storage") {
    return "#111d5e"
  } else return "#fff"
}

export const formatMoney = (n, c, d, t, currency, allow0 = false) => {
  var c = isNaN(c = Math.abs(c)) ? 2 : c,
      d = d === undefined ? "." : d,
      t = t === undefined ? "," : t,
      s = n < 0 ? "-" : "",
      i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
      j = (j = i.length) > 3 ? j % 3 : 0;
  if(i <= 0 && !allow0)
      return "-";
  else
      return currency + s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

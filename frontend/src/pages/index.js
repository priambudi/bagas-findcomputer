import React, { useState, useEffect } from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import ModalAddItem from "../components/modalAddItem"
import ItemCards from "../components/itemCards"
import Image from "../components/image"
import { CenterText } from "../components/centerText"
const axios = require("axios").default

const IndexPage = () => {
  const [item, setItem] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  useEffect(() => {
    setIsLoading(true)
    axios
      .get(`${process.env.GATSBY_BACKEND_URL}/item/all`, { timeout: 5000 })
      .then(function (response) {
        console.log(response)
        setItem(response.data)
      })
      .catch(function (error) {
        console.log(error)
      })
      .finally(()=>{
        setIsLoading(false)
      })
  }, [])
  return (
    <Layout>
      <SEO title="Home" />
      <div className="row" style={{ marginTop: "10vh" }}>
        {isLoading ? <CenterText text={"Loading"} /> : <ItemCards data={item} />}
      </div>
      <ModalAddItem />
    </Layout>
  )
}

export default IndexPage

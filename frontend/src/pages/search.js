import React, { useState, useEffect } from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import ItemCards from "../components/itemCards"
import { Dropdown } from "../components/dropdown"

const axios = require("axios").default
const SearchPage = () => {
  const [query, setQuery] = useState("")
  const [item, setItem] = useState([])
  const [categories, setCategories] = useState([])
  const [category, setCategory] = useState(0)
  useEffect(() => {
    if (query) {
      axios
        .get(`${process.env.GATSBY_BACKEND_URL}/item/search?q=${query}&c=${category}`, { timeout: 5000 })
        .then(function (response) {
          console.log(response)
          setItem(response.data)
        })
        .catch(function (error) {
          console.log(error)
        })
    }
  }, [query, category])

  useEffect(() => {
    fetch(`${process.env.GATSBY_BACKEND_URL}/category/all`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
      .then(response => response.json()) // parse JSON from request
      .then(resultData => {
        console.log(resultData)
        setCategories(resultData)
      })
  }, [])

  return (
    <Layout>
      <SEO title="search" />
      <div className="real-center" style={{ paddingTop: '10px', position: 'fixed', zIndex: 1000, }}>
        <div className="form-group" style={{ backgroundColor: 'white', zIndex: 999 }}>
          <input
            onChange={e => setQuery(e.target.value)}
            placeholder={"Search for RAM, Motherboard, lover?"}
            className="input-block"
            type="text"
            id="paperInputs3"
          />
        </div>
      </div>
      <div style={{ marginTop: "10vh" }}>
        <label htmlFor="inputCategory">Category</label>
        <select id="inputCategory" onChange={(e) => setCategory(e.target.value)} >
          <Dropdown items={categories} defaultText={"All"} showDefault={true} />
        </select>
      </div>
      <div className="row" style={{ marginTop: 0 }}>
        <ItemCards data={item} />
      </div>
    </Layout>
  )
}

export default SearchPage

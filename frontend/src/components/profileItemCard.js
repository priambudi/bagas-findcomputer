import React, { Fragment } from "react"
import { navigate } from "gatsby"
import { parseCategoryToColor } from "../utilities/utilities"
import { Price } from "./price"
const axios = require("axios").default
var querystring = require("querystring")

const ProfileItemCard = ({ id, title, category, price, description, owner, stock }) => {
  const deviceName = typeof Storage !== "undefined"
    ? JSON.parse(localStorage.getItem("u")) ? JSON.parse(localStorage.getItem("u")).name : ''
    : ""
  console.log(deviceName, owner)
  
  return (
    <div key={id} className="sm-6 md-4 col">
      <div className="card" style={{ width: "15rem" }}>
        <div className="card-body">
          <h4 className="card-title">
            <span
              className="badge"
              style={{ backgroundColor: parseCategoryToColor(category) }}
            >
              {category}
            </span>{" "}
            {title}
          </h4>
          <Price price={price} stock={stock} />
          <p id={"item-description-"+id} className="card-text">{description}</p>
          <div className="row flex-middle flex-edges">
            <div className="col-8">{owner}</div>
            <div className="col-3">
            
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProfileItemCard

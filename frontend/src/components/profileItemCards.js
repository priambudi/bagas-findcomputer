import React from "react"
import ProfileItemCard from "./profileItemCard";

const ProfileItemCards = ({ data }) =>
    data.map(({ id, name, category, price, description, owner, stock }) => {
        return <ProfileItemCard id={id} title={name} category={category} price={price} description={description} owner={owner} stock={stock} />
    })

export default ProfileItemCards;
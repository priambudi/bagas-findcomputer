import React from "react"

export const CenterText = ({text}) =>{
    return <h2 style={{marginLeft: 'auto', marginRight: 'auto', marginTop: '30vh'}}>{text}</h2>
}
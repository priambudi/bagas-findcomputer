import React from "react"
import ItemCard from "./itemCard"
import { CenterText } from "./centerText"

const ItemCards = ({ data }) => {
    if (data.length > 0) {
        return data.map(({ id, name, category, price, description, owner, stock }) => {
            return <ItemCard id={id} title={name} category={category} price={price} description={description} owner={owner} stock={stock} />
        })
    } else {
        return <CenterText text={"we're out of stock"} />
    }
}

export default ItemCards;
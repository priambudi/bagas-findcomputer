import React, { Fragment } from "react"

export const Dropdown = ({items, defaultText = "Choose", showDefault = false}) =>{
    return <Fragment>
        <option selected disabled={!showDefault} hidden={!showDefault} value="0">{defaultText}</option>
        <Dropdowns items={items} />
    </Fragment>
}

export const Dropdowns = ({items}) =>{
    return items.map((item, index)=>{
        const optionId = item.id || index+1;
        return <option key={optionId} id={item.name.split(' ').join('-')} value={optionId}>{item.name}</option>
    })
}
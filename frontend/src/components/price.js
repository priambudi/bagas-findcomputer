import React from "react"
import { formatMoney } from "../utilities/utilities"

export const Price = ({price, stock}) =><div className="row flex-middle"><h5 className="card-subtitle" style={{ color: '#000', textDecoration: stock > 0 ? 'none' : 'line-through red' }}>{formatMoney(price,0,',','.',"Rp")}</h5><span className="margin-left-small">{stock > 0 ? '' : 'Sold Out'}</span></div>
import React from "react"
import { parseCategoryToColor } from "../utilities/utilities"
import { Price } from "./price"
const axios = require("axios").default
var querystring = require("querystring")

const ItemCard = ({ id, title, category, price, description, owner, stock }) => {
  const deviceName = typeof Storage !== "undefined"
    ? JSON.parse(localStorage.getItem("u")) ? JSON.parse(localStorage.getItem("u")).name : ''
    : ""
  const newOwner = `${owner} ${owner === deviceName ? '(You)' : ''}`
  return (
    <div key={id} className="sm-6 md-4 col">
      <div
        className="card" 
        style={{ width: "15rem", pointerEvents: JSON.parse(localStorage.getItem("u")) && stock > 0 && deviceName !== owner ? 'default' : 'none', boxShadow: JSON.parse(localStorage.getItem("u")) && stock > 0 && deviceName !== owner ? 'box-shadow: 15px 28px 25px -18px rgba(0,0,0,0.2)' : 'none' }}
      >
        <div className="card-body">
          <h4 className="card-title">
            <span
              className="badge"
              style={{ backgroundColor: parseCategoryToColor(category) }}
            >
              {category}
            </span>{" "}
            {title}
          </h4>
          <Price price={price} stock={stock} />
          <p id={"item-description-"+id} className="card-text">{description}</p>
          <div className="row flex-middle flex-edges">
            <div className="col-8">{newOwner}</div>
            <div className="col-3">
              {JSON.parse(localStorage.getItem("u")) && stock > 0 && deviceName !== owner ?
                <button onClick={() => {
                  axios
                    .post(
                      `${process.env.GATSBY_BACKEND_URL}/item/delete`,
                      querystring.stringify({
                        itemId: id,
                      }),
                      {
                        headers: {
                          "Content-Type": "application/x-www-form-urlencoded",
                        },
                      }
                    )
                    .then(res => {
                      console.log(res)
                      window.location.reload()
                    })
                    .catch(alert)
                }} aria-label="buy">Buy</button>
                : ''}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ItemCard

import React, { Fragment, useEffect, useState } from "react"
import { Dropdown } from "./dropdown"
import { navigate } from "gatsby"
const axios = require("axios").default
var querystring = require("querystring")

const ModalUpdateItem = (props) => {
    const deviceId = typeof Storage !== "undefined"
        ? JSON.parse(localStorage.getItem("u")) ? JSON.parse(localStorage.getItem("u")).id : ''
        : ""
    const [chosenItemId, setChosenItemId] = useState('')
    const [id, setId] = useState('')
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [category, setCategory] = useState('')
    const [categoryId, setCategoryId] = useState('')
    const [price, setPrice] = useState('')
    const [categories, setCategories] = useState([])

    useEffect(() => {
        fetch(`${process.env.GATSBY_BACKEND_URL}/category/all`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                "Content-Type": "application/x-www-form-urlencoded",
            },
        })
            .then(response => response.json()) // parse JSON from request
            .then(resultData => {
                console.log(resultData)
                setCategories(resultData)
            })
    }, [])

    useEffect(() => {
        console.log(chosenItemId)
        fetch(`${process.env.GATSBY_BACKEND_URL}/item/detail`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                "Content-Type": "application/x-www-form-urlencoded",
            },
            body: querystring.stringify({
                id: chosenItemId,
            })
        })
            .then(response => response.json()) // parse JSON from request
            .then(resultData => {
                setId(resultData.id)
                setName(resultData.name)
                setDescription(resultData.description)
                setCategory(resultData.category)
                setCategoryId(resultData.categoryId)
                setPrice(resultData.price)
            })
    }, [chosenItemId])
    return (
        <Fragment>
            <div className="add-item" id="update-item">
                <label className="paper-btn margin" for="modal-2">Update Item</label>
            </div>
            <input className="modal-state" id="modal-2" type="checkbox" />
            <div className="modal">
                <div className="modal-body">
                    <label className="btn-close" for="modal-2">X</label>
                    <h4 className="modal-title">Update Item</h4>
                    <form id="register-form">
                        <div className="col sm-6">
                            <div className="form-group">
                                <label htmlFor="inputCategory">Item</label>
                                <select id="inputCategory" onChange={(e) => setChosenItemId(e.target.value)} >
                                    <Dropdown items={props.items} />
                                </select>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col sm-6">
                                <div className="form-group">
                                    <label htmlFor="inputName">Name</label>
                                    <input
                                        value={name}
                                        onChange={e => setName(e.target.value)}
                                        type="text"
                                        className="input-block"
                                        placeholder="Input your name here"
                                        id="inputNameBarang"
                                    />
                                </div>
                            </div>
                            <div className="col sm-6">
                                <div className="form-group">
                                    <label htmlFor="inputDescription">Description</label>
                                    <input
                                        value={description}
                                        onChange={e => setDescription(e.target.value)}
                                        type="text"
                                        className="input-block"
                                        placeholder="description"
                                        id="inputDescription"
                                    />
                                </div>
                            </div>
                            <div className="col sm-6">
                                <div className="form-group">
                                    <label htmlFor="inputCategory">Category</label>
                                    <select id="inputCategory" value={category} onChange={(e) => setCategory(e.target.value)} >
                                        <Dropdown items={categories} />
                                    </select>
                                </div>
                            </div>
                            <div className="col sm-6">
                                <div className="form-group">
                                    <label htmlFor="inputPrice">Price</label>
                                    <input
                                        value={price}
                                        onChange={e => setPrice(e.target.value)}
                                        type="number"
                                        placeholder=""
                                        id="inputPrice"
                                    />
                                </div>
                            </div>
                        </div>
                    </form>
                    <div className="row">
                            <div className="col">
                            <button
                        id="updateItem-submit"
                        // disabled={!(name && description && category && price)}
                        onClick={() => {
                            axios
                                .post(
                                    `${process.env.GATSBY_BACKEND_URL}/item/update`,
                                    querystring.stringify({
                                        id,
                                        name,
                                        description,
                                        categoryId,
                                        ownerId: deviceId,
                                        price
                                    }),
                                    {
                                        headers: {
                                            "Content-Type": "application/x-www-form-urlencoded",
                                        },
                                    }
                                )
                                .then(res => {
                                    console.log(res)
                                })
                                .catch(alert)
                                .finally(() => {
                                    // navigate("/profile")
                                    window.location.reload()
                                })
                        }}
                        aria-label="register"
                        className="background-primary"
                        type="button"
                        form="register-form"
                    >
                        Update
                    </button>
                            </div>
                            <div className="col">
                            <button
                        id="updateItem-submit"
                        // disabled={!(name && description && category && price)}
                        onClick={() => {
                            axios
                    .post(
                      `${process.env.GATSBY_BACKEND_URL}/item/delete`,
                      querystring.stringify({
                        itemId: id,
                      }),
                      {
                        headers: {
                          "Content-Type": "application/x-www-form-urlencoded",
                        },
                      }
                    )
                    .then(res => {
                      console.log(res)
                      window.location.reload()
                    })
                    .catch(alert)
                        }}
                        aria-label="register"
                        className="background-primary"
                        type="button"
                        form="register-form"
                    >
                        Delete
                    </button>
                            </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default ModalUpdateItem

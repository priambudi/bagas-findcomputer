package bagas.FindComputer;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import bagas.FindComputer.controller.CategoryController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryTest {

    @Autowired
    private CategoryController categoryController;

    @Test
    public void contextLoads() throws Exception {
        assertThat(categoryController).isNotNull();
    }

    @Test
    public void getAll() throws Exception {
        assertThat(categoryController.getAllCategories().size())
                .isGreaterThan(0);
    }

}

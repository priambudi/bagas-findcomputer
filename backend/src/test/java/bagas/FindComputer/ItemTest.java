package bagas.FindComputer;

import bagas.FindComputer.controller.ItemController;
import bagas.FindComputer.controller.UserController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ItemTest {

    @Autowired
    private ItemController itemController;

    @Test
    public void contextLoads() throws Exception {
        assertThat(itemController).isNotNull();
    }

}

package bagas.FindComputer.repository;

import bagas.FindComputer.model.Category;
import bagas.FindComputer.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends CrudRepository<Category, Integer> {
    List<Category> findAll();
}

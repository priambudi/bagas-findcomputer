package bagas.FindComputer.repository;

import bagas.FindComputer.model.Item;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface ItemRepository extends CrudRepository<Item, Integer> {
    @Query("select i from Item i where i.name = :name")
    List<Item> findByName(String name);

    @Query("select i from Item i where i.id = :id")
    Item findByItemId(Integer id);

    @Query("select i from Item i where i.categoryId = :id")
    List<Item> findByCategoy(Integer id);

    @Query("select i from Item i where i.ownerId = :id and i.stock = 1")
    List<Item> findByUserId(Integer id);

    @Query("select i from Item i where i.name = :name and i.categoryId = :id")
    List<Item> findByNameAndCategory(String name, Integer id);

    @Query("select i from Item i where i.stock = 1")
    List<Item> findAllAvailableItem();
}

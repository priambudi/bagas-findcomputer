package bagas.FindComputer.controller;

import bagas.FindComputer.model.Category;
import bagas.FindComputer.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/category")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping(path="/all")
    public @ResponseBody
    List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }
}
package bagas.FindComputer.controller;

import bagas.FindComputer.model.Item;
import bagas.FindComputer.model.ItemCard;
import bagas.FindComputer.repository.CategoryRepository;
import bagas.FindComputer.repository.ItemRepository;
import bagas.FindComputer.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/api/v1/item")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ItemController {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private UserRepository userRepository;

    @PostMapping(
            path="/add",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}
            )
    public String addItem(Item i) throws Exception {
        try {
            itemRepository.save(i);
            return "Item saved";
        } catch (
                Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(
            path="/delete",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}
    )
    public String removeItem(@RequestParam Map<String, String> paramMap) {
        Integer itemId = Integer.parseInt(paramMap.get("itemId"));
        try {
            Item i = itemRepository.findById(itemId).get();
            i.setStock(0);
            itemRepository.save(i);
            return "Item removed";
        } catch (
                Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(
            path="/update",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}
    )
    public Item update(@RequestParam Map<String, String> paramMap) {
        try {
            Integer id = Integer.parseInt(paramMap.get("id"));
            Item i = itemRepository.findByItemId(id);
            paramMap.forEach((k, v) -> {
                if (k.equals("name")) {
                    i.setName(v);
                } if (k.equals("description")) {
                    i.setDescription(v);
                } if (k.equals("categoryId")) {
                    i.setCategoryId(Integer.parseInt(v));
                } if (k.equals("price")) {
                    i.setPrice(Integer.parseInt(v));
                }
            });
            itemRepository.save(i);
            return i;
        } catch (
                Exception e) {
            throw new ResponseStatusException(HttpStatus.OK);
        }
    }

    @GetMapping(path="/search")
    public @ResponseBody List<ItemCard> search(@RequestParam Map<String, String> paramMap) {
        String query = paramMap.get("q");
        Integer categoryId = Integer.parseInt(paramMap.get("c"));
        try {
            if(categoryId > 0) {
                return convertItemsToItemCards(itemRepository.findByNameAndCategory(query, categoryId));
            } else {
                return convertItemsToItemCards(itemRepository.findByName(query));
            }
        } catch (
                Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path="/filter")
    public @ResponseBody List<ItemCard> filterByCategory(@RequestParam Map<String, String> paramMap) {
        Integer categoryId = Integer.parseInt(paramMap.get("categoryId"));
        try {
            return convertItemsToItemCards(itemRepository.findByCategoy(categoryId));
        } catch (
                Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path="/user",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public
    List<ItemCard> getUserItems(@RequestParam Map<String, String> paramMap) {
        Integer id = Integer.parseInt(paramMap.get("id"));
        try {
            return convertItemsToItemCards(itemRepository.findByUserId(id));
        } catch (
                Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path="/detail",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public
    ItemCard getItemDetail(@RequestParam Map<String, String> paramMap) {
        Integer id = Integer.parseInt(paramMap.get("id"));
        try {
            return convertItemToItemCard(itemRepository.findByItemId(id));
        } catch (
                Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path="/all")
    public @ResponseBody
    ResponseEntity<Iterable<ItemCard>> getAllItems() {
        return new ResponseEntity<>(convertItemsToItemCards(itemRepository.findAllAvailableItem()), HttpStatus.OK);
    }

    public List<ItemCard> convertItemsToItemCards(Iterable<Item> iterableItems) {
        List<Item> items = StreamSupport
                .stream(iterableItems.spliterator(), false)
                .collect(Collectors.toList());
        return items.stream()
                .map(this::convertItemToItemCard)
                .collect(Collectors.toList());
    }

    private ItemCard convertItemToItemCard(Item item) {
        ItemCard itemCard = new ItemCard(item);
        String category = categoryRepository
                .findById(item.getCategoryId())
                .get()
                .getName();
        String owner = userRepository
                .findById(item.getOwnerId())
                .get()
                .getName();
        itemCard.setCategory(category);
        itemCard.setOwner(owner);
        return itemCard;
    }
}
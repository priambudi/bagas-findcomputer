package bagas.FindComputer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FindComputerApplication {

    public static void main(String[] args) {
        SpringApplication.run(FindComputerApplication.class, args);
    }

}